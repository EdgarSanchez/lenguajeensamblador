%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    mensaje db 'leer archivo correcto', 10
    len equ $-mensaje
    path db "/home/camila/Documentos/Ensamblador/Practicas/leer_archivo.txt", 0
section .bss
    texto resb 30
    idarchivo resd 1
section .text
    global _start
    
_start:
    ; apertura del archivo
    mov eax, 5 ; servicio para abrir archivos
    mov ebx, path ; servicio para la direccion del archivo
    mov ecx, 0     ; modo de acceso , solo leer ; 1=escritura ; 2=lectura escritura
    mov edx, 0     ; permisos del archivo
    int 80h
    ; 
    test eax, eax ; para verificar que la dirección del archivo esté bien 
    jz salir
    ; ########## archivo sin excepciones ###########
    mov dword [idarchivo], eax ; respaldo el id del archivo
    imprimir mensaje, len
    mov eax, 3 ; lectura de archivo
    mov ebx, [idarchivo] ; entrada estandar para el archivo
    mov ecx, texto 
    mov edx, 25
    int 80h
    
    imprimir texto, 25
    
    ; cierre del archivo
    mov eax, 6 ; servicio para cerrar archivos
    mov edx, [idarchivo] ; 
    mov ecx, 0     
    mov edx, 0     
    int 80h
salir:
    mov eax, 1
    int 80h
    