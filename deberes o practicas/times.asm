section .data
	msg: times 7 db '*',0xa
	len: equ $-msg

section .text
	global _start

_start:
	mov eax,04
	mov ebx,01
	mov ecx,msg
	mov edx,len
	int 80H

	mov eax,1
	mov ebx,0
	int 80H
