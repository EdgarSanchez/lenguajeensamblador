section .data
	msj_a: db 'Ingrese el primer valor:',10
	len_a: equ $-msj_a
	msj_b: db 10,'Ingrese el segundo valor:',10
	len_b: equ $-msj_b
	msj_s: db 10,'Sumar: 1'
	len_s: equ $-msj_s
	msj_rest: db 10,'Restar: 2'
	len_rest: equ $-msj_rest
	msj_m: db 10,'Multiplicar: 3'
	len_m: equ $-msj_m
	msj_d: db 10,'Dividir: 4',10
	len_d: equ $-msj_d
	msj_e: db 10,'Elija un valor',10
	len_e: equ $-msj_e
	msj_dlg: db 10,'Elija un valor valido',10
	len_dlg: equ $-msj_d
	msj_r: db 10,'Resultado ****'
	len_r: equ $-msj_r
	msj_f: db 10,' ',10
	len_f: equ $-msj_f
	
section .bss
	a: resb 1
	b: resb 1
	r: resb 1
	e: resb 1
	x: resb 1
	
section .text
	global _start
	
_start:

	lectura_a:
		mov eax,04
		mov ebx,01
		mov ecx,msj_a
		mov edx,len_a
		int 80H
	
		mov eax,03
		mov ebx,02
		mov ecx,a
		mov edx,2
		int 80H
		
		jmp lecrtura_b
		
	lecrtura_b:
		mov eax,04
		mov ebx,01
		mov ecx,msj_b
		mov edx,len_b
		int 80H
	
		mov eax,03
		mov ebx,02
		mov ecx,b
		mov edx,2
		int 80H
		
		jmp switch
		
	switch:
		mov eax,04
		mov ebx,01
		mov ecx,msj_s
		mov edx,len_s
		int 80H
		
		mov eax,04
		mov ebx,01
		mov ecx,msj_rest
		mov edx,len_rest
		int 80H
		
		mov eax,04
		mov ebx,01
		mov ecx,msj_m
		mov edx,len_m
		int 80H
		
		mov eax,04
		mov ebx,01
		mov ecx,msj_d
		mov edx,len_d
		int 80H
		
		mov eax,04
		mov ebx,01
		mov ecx,msj_e
		mov edx,len_e
		int 80H
		
		mov eax,03
		mov ebx,02
		mov ecx,e
		mov edx,2
		int 80H
		
		jmp comp_suma
		
	comp_suma:
		mov al,[e]
		sub al,'0'
		cmp al,1
		jz suma
		jmp comp_resta
		
	comp_resta:
		mov al,[e]
		sub al,'0'
		cmp al,2
		jz resta
		jmp comp_mult
		
	comp_mult:
		mov al,[e]
		sub al,'0'
		cmp al,3
		jz mult
		jmp comp_div
		
	comp_div:
		mov al,[e]
		sub al,'0'
		cmp al,1
		jz divs
		jmp show_msj
		
	show_msj:
		mov eax,04
		mov ebx,01
		mov ecx,msj_dlg
		mov edx,len_dlg
		int 80H
		
		jmp switch

	suma:
		mov [msj_r+11], dword 'sum:'
		mov ax,[a]
		mov bx,[b]
		sub ax,'0'
		sub bx,'0'
		add ax,bx
		add ax,'0'
		mov [r],ax
		
		mov eax,04
		mov ebx,01
		mov ecx,msj_r
		mov edx,len_r
		int 80H
		
		mov eax,04
		mov ebx,01
		mov ecx,r
		mov edx,1
		int 80H
		
		jmp salir
		
	resta:
		mov [msj_r+11], dword 'res:'
		mov ax,[a]
		mov bx,[b]
		sub ax,'0'
		sub bx,'0'
		sub ax,bx
		add ax,'0'
		mov [r],ax
		
		mov eax,04
		mov ebx,01
		mov ecx,msj_r
		mov edx,len_r
		int 80H
		
		mov eax,04
		mov ebx,01
		mov ecx,r
		mov edx,1
		int 80H
		
		jmp salir
		
	mult:
		mov [msj_r+11], dword 'mul:'
		mov al,[a]
		mov bl,[b]
		sub al,'0'
		sub bl,'0'
		mul bl
		add ax,'0'
		mov [r],ax
		
		mov eax,04
		mov ebx,01
		mov ecx,msj_r
		mov edx,len_r
		int 80H
		
		mov eax,04
		mov ebx,01
		mov ecx,r
		mov edx,1
		int 80H
		
		jmp salir
		
	divs:
		mov [msj_r+11], dword 'div:'
		mov al,[a]
		mov bl,[b]
		sub al,'0'
		sub bl,'0'
		div bl
		add al,'0'
		add ah,'0'
		mov [r],al
		mov [x],ah
		
		mov eax,04
		mov ebx,01
		mov ecx,msj_r
		mov edx,len_r
		int 80H
		
		mov eax,04
		mov ebx,01
		mov ecx,r
		mov edx,1
		int 80H
		jmp salir
		
	salir:
		mov eax,04
		mov ebx,01
		mov ecx,msj_f
		mov edx,len_f
		int 80H
	
		mov eax,01
		mov ebx,00
		int 80H