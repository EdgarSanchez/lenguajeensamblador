section .data
	msj: db 'Ingrese su nombre:', 0xA
	len: equ $-msj
	presentar: db 'nombre ingresado', 0xA
	len_p: equ $-presentar
	
section .bss
	nombre: resb 10
	
section .text
	global _start
	
_start:
	mov eax,04
	mov ebx,01
	mov ecx,msj
	mov edx,len
	int 80h
	
	mov eax,03
	mov ebx,02
	mov ecx,nombre
	mov edx,10
	int 80H
	
	mov eax,04
	mov ebx,01
	mov ecx,presentar
	mov edx,len_p
	int 80h
	
	mov eax,04
	mov ebx,01
	mov ecx,nombre
	mov edx,10
	int 80h
	
	mov eax,01
	mov ebx,00
	int 80h