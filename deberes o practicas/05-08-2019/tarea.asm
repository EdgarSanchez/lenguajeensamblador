; Edgar Patricio Sánchez Malla
; Ciclo-Paralelo: 6 "B"
%macro escribir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80H
%endmacro
section .data
	mensaje db "La resta es: "
	leng_mensaje equ $ -mensaje

	mensaje2 db "La suma es: "
	leng_mensaje2 equ $ -mensaje2

	salto db "",10
	leng_salto equ $ -salto

	pathNum1 db "/home/edgar/Documentos/Asemblerprojects/05-08-2019/archivo1.txt", 0
	pathNum2 db "/home/edgar/Documentos/Asemblerprojects/05-08-2019/archivo2.txt", 0

	resta db "   "
	suma db "   "

section .bss
	n1 resb 3
	n2 resb 3
	idarchivo resd 1

section .text
	global _start
_start:
	mov eax, 5		    ;Es el servicio para abrir el archivo
	mov ebx, pathNum1 	;Dirección del archivo
	mov ecx, 0		    ;Modo de acceso
	mov edx, 0		    ;Permisos del archivo
	int 80H

	test eax, eax       ;comprueba el estado y si hay un error simplemete modifica 
	;las banderas, no modifica los registros
	jz salir

	;******Guardar el archivo y su id***********
	mov dword [idarchivo], eax
	;Leer el archivo
	mov eax, 3
	mov ebx, [idarchivo]
	mov ecx, n1
	mov edx, 3
	int 80H

	;Todos estos datos los encontramos en la documentación de Int 80H
	mov eax, 5		;Es el servicio para abrir el archivo
	mov ebx, pathNum2 	;Dirección del archivo
	mov ecx, 0		;Modo de acceso
	mov edx, 0		;Permisos del archivo
	int 80H

	test eax, eax ;comprueba el estado y si hay un error simplemete modifica 
	;las banderas, no modifica los registros
	jz salir

	;******Guardar el archivo y su id***********
	mov dword [idarchivo], eax
	;Leer el archivo
	mov eax, 3
	mov ebx, [idarchivo]
	mov ecx, n2
	mov edx, 3
	int 80H
;resta
	mov ecx, 3	;número de operaciones de resta
	mov esi, 2	;posición del dígito en cada cadena n1 y n2
	clc 		;Coloca la bandera de Carry en cero
loop_resta:
	mov al, [n1+esi]
	sbb al, [n2+esi] ;resta el acarreo de la resta  en binario
	aas ;Ajusta la operación binaria al sistema decimal
	pushf
	or al, 30h
	popf
	mov [resta+esi], al
	dec esi

	loop loop_resta

	escribir mensaje, leng_mensaje
	escribir resta, 3 
	escribir salto, leng_salto

; suma
	mov ecx, 3	;número de operaciones de resta
	mov esi, 2	;posición del dígito en cada cadena n1 y n2
	clc 		;Coloca la bandera de Carry en cero
ciclo_suma:
    mov al, [n1+esi] ; 
    adc al, [n2+esi]
    aaa ; ajuste
    pushf ; guarda en pila todos los valores de las banderas
    
    or al, 30h ; add 48 o add '0'
    popf ; rescata el estado de las banderas
    
    mov [suma+esi], al
    dec esi
    loop ciclo_suma

    escribir mensaje2, leng_mensaje2
	escribir suma, 3
	escribir salto, leng_salto


salir: 
	mov eax, 1
	int 80h