section .data
    mensaje db 'Ingrese 5 numeros y presiona enter', 10
    len equ $-mensaje
    
    arreglo db 0,0,0,0,0
    lenArreglo equ $-arreglo

section .bss
    resultado resb 2; numero +enter
section .text
    global _start
_start:
    mov esi, arreglo ; indice fuente
    mov edi, 0       ; indice destino 
    
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, len
    int 80h
    
    ; leer resultado
leer:
    mov eax, 3
    mov ebx, 1
    mov ecx, resultado
    mov edx, 2
    int 80h
    
    mov al, [resultado]
    sub al, '0'
    
    ; posicion 
    mov [esi], al
    inc esi
    inc edi
    cmp edi, lenArreglo
    jb leer ; salta cuando cf=1
    mov ecx, lenArreglo - 1
        
imprimir:
    push ecx
    mov al, [arreglo+ecx]
    add al, '0'
    mov [resultado], al
    
    
    mov eax, 4
    mov ebx, 1
    mov ecx, resultado
    mov edx, 1
    int 80h
    
    pop ecx  
    
    
    dec ecx
    cmp ecx, lenArreglo
    jb imprimir
    

salir:  
    mov eax, 1
    int 80h