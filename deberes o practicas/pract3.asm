section .data
	msj: db 'Hola clase', 0xA
	len: equ $-msj

section .text
	global _start
	
_start:
	mov eax,04
	mov ebx,01
	mov ecx,msj
	mov edx,len
	int 80h
	
	mov [msj+0], dword 'mala'
	mov eax,04
	mov ebx,01
	mov ecx,msj
	mov edx,len
	int 80H
	
	mov [msj+5], dword 'nina'
	mov eax,04
	mov ebx,01
	mov ecx,msj
	mov edx,len
	int 80H
	
	mov eax,01
	mov ebx,00
	int 80h
