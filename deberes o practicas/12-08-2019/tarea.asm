;Edgar Patricio Sánchez Malla
;6 "B"
%macro imprimir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80H
%endmacro
section .data
    path1 db "/home/edgar/Documentos/Asemblerprojects/12-08-2019/arreglo.txt", 0
    path2 db "/home/edgar/Documentos/Asemblerprojects/12-08-2019/repeticiones.txt", 0
    suma db 'x=x veces,x=x veces,x=x veces,x=x veces,x=x veces,x=x veces,x=x veces,x=x veces,x=x veces,x=x veces'
    len_suma equ $- suma
section .bss
    primero resb 10
    segundo resb 1

    tercero resb 5
    idarchivo resd 1

    aux1 resb 2
    aux2 resb 2
    contador resb 2
    dato resb 2

    indice resb 2

section .text
    global _start
_start:
	mov al,0
	mov [aux1],al
	mov al,len_suma-7
	mov [indice],al

	mov eax, 5		
	mov ebx, path1 	
	mov ecx, 0		
	mov edx, 0		
	int 80H

	test eax, eax 
	;-------Guardar el archivo y la id----------
	mov dword [idarchivo], eax

	;Leer el archivo
	mov eax, 3
	mov ebx, [idarchivo]
	mov ecx, primero
	mov edx, 10
	int 80H

	; cierre del archivo
    mov eax, 6 
    mov edx, [idarchivo] ; 
    mov ecx, 0     
    mov edx, 0     
    int 80h
    
    mov ecx, 10
	mov esi, 0
	clc 
    
loop1:
    push ecx
    dec ecx
    mov [aux1],ecx
    mov ecx,10

    loop2:
    push ecx
    mov bl,[aux1]
    dec ecx
    mov al,[primero+ecx]
    mov bl,[primero+ebx]

    cmp al,bl
    jz comparar

    finLoop2:

    pop ecx
    loop loop2

finLoop1:
	call almacenar

	mov al,0
	mov [contador],al

    pop ecx
    loop loop1
    jmp salir


salir: 
	call guardarArchivo
  
	mov eax, 1
	int 80h

comparar:
	mov [dato],al
	mov al,[contador]
	inc al
	mov	[contador],al

	jmp finLoop2

almacenar:
	mov al,[dato]
	mov bl,[contador]
	add bl,'0'

	mov ecx,[indice]

	mov [suma+ecx],bl
	sub ecx,2
	mov [suma+ecx],al
	sub ecx,8
	mov [indice],ecx
	
ret
	
guardarArchivo:
	mov eax, 5		
	mov ebx, path2 	
	mov ecx, 1		
	mov edx, 0		
	int 80H

	test eax, eax 

	;-------Guardar el archivo y la id----------
	mov dword [idarchivo], eax

	;Leer el archivo
	mov eax, 4
	mov ebx, [idarchivo]
	mov ecx, suma
	mov edx, len_suma
	int 80H


	; cierre del archivo
    mov eax, 6 
    mov edx, [idarchivo] ; 
    mov ecx, 0     
    mov edx, 0     
    int 80h

ret