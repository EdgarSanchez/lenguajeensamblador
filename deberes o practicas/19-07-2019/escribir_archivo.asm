%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    mensaje db 'Escribir archivo', 10
    len equ $-mensaje
    path db "/home/edgar/Documentos/Asemblerprojects/12-08-2019/repeticiones.txt", 0
section .bss
    texto resb 30
    idarchivo resd 1
section .text
    global _start
    
_start:
    ; apertura del archivo
    mov eax, 8 ; servicio para crear y escribbir en archivos
    mov ebx, path ; servicio para la direccion del archivo
    mov ecx, 2     ; modo de acceso , solo leer 1=write ; 2=read and write
    mov edx, 200h     ; permisos del archivo
    int 80h
    ; 
    test eax, eax ; por que puede existir una excepcion -es un and sin modificar sus operandos, solo modifica banderas
    jz salir
    ; ########## archivo sin excepciones ###########
    mov dword [idarchivo], eax ; respaldo el id del archivo    
    imprimir mensaje, len
    mov eax, 3
    mov ebx, 2
    mov ecx, texto
    mov edx, 15
    int 80h
    
    
    mov eax, 4
    mov ebx, [idarchivo] ; entrada estandar para el archivo
    mov ecx, texto
    mov edx, 15
    int 80h
    
    ; cierre del archivo
    mov eax, 6 ; servicio para cerrar archivos
    mov edx, [idarchivo] ; 
    mov ecx, 0     
    mov edx, 0     
    int 80h
salir:
    mov eax, 1
    int 80h
    