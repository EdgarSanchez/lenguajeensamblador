%macro imprimir 2 ; 2-> hace referencia a los paramatros a recibir
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    mensaje1 db 'Inicio de ciclo', 10, 'realizado por:', 10
    len1  equ $-mensaje1
    mensaje2 db 'Edgar', 10
    len2  equ $-mensaje2
    mensaje3 db 'fin de ciclo', 10
    len3  equ $-mensaje3
section .text
    global _start
_start:
    imprimir mensaje1, len1
    mov ecx, 9
linea1:
    push ecx
    imprimir mensaje2, len2
    pop ecx
    loop linea1
linea2:
    imprimir mensaje3, len3
    mov eax, 1
    int 80h