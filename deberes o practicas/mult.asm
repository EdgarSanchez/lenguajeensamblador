section .data
	msj_a: db 'Ingerse el primer valor:',0xA
	len_a: equ $-msj_a
	msj_b: db 10, 'Ingerse el segundo valor:',0xA
	len_b: equ $-msj_b
	msj_r: db 10, 'El resultado es:',0xA
	len_r: equ $-msj_r
	msj_f: db 10, '*****************',0xA
	len_f: equ $-msj_r
	
section .bss
	a: resb 1
	b: resb 1
	r: resb 1
	
section .text
	global _start
	
_start:
	mov eax,04
	mov ebx,01
	mov ecx,msj_a
	mov edx,len_a
	int 80H
	
	mov eax,03
	mov ebx,02
	mov ecx,a
	mov edx,2
	int 80H
	
	mov eax,04
	mov ebx,01
	mov ecx,msj_b
	mov edx,len_b
	int 80H
	
	mov eax,03
	mov ebx,02
	mov ecx,b
	mov edx,2
	int 80H
	
	mov al,[a]
	mov bl,[b]
	sub al,'0'
	sub bl,'0'
	mul bl
	add ax,'0'
	mov [r],ax
	
	mov eax,04
	mov ebx,01
	mov ecx,msj_r
	mov edx,len_r
	int 80H
	
	mov eax,04
	mov ebx,01
	mov ecx,r
	mov edx,1
	int 80H
	
	mov eax,04
	mov ebx,01
	mov ecx,msj_f
	mov edx,len_f
	int 80H
	
	mov eax,01
	mov ebx,00
	int 80H