section .data
	materia: db 'Lenguaje ensamblador', 0xa
	curso: db 'Sexto Ciclo B', 0xa
	nombre: db 'Luis Paredes', 0xa
	len1: equ $-materia
	len2: equ $-curso
	len3: equ $-nombre
	

section .text
	global _start

_start:
	mov eax,04	;ax define el tipo de operación del S.O(4=write)
	mov ebx,01	;bx define la unidad estandar
	mov ecx,materia ;cx se almacena la referencia de la constante saludo para imprimir
	mov edx,len1	;dx define el número de caracteres que se va a imprimir
	int 80H		;interrupción
	
	mov eax,1	;ax define el tipo de operación (1=exit)
	mov ebx,0
	int 80H

	mov eax,04	;ax define el tipo de operación del S.O(4=write)
	mov ebx,01	;bx define la unidad estandar
	mov ecx,curso ;cx se almacena la referencia de la constante saludo para imprimir
	mov edx,len2	;dx define el número de caracteres que se va a imprimir
	int 80H		;interrupción
	
	mov eax,1	;ax define el tipo de operación (1=exit)
	mov ebx,0
	int 80H
	
	mov eax,04
	mov ebx,01
	mov ecx,nombre
	mov edx,len2
	int 80H
	
	mov eax,1	;ax define el tipo de operación (1=exit)
	mov ebx,0
	int 80H