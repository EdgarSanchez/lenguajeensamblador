;El call es un llamado a subrutinas
;loop es una instruccionde ciclos
section .data
    asterisco db '*'
    nueva_linea db 10,''

section .text
    global _start

_start:
    mov ecx, 20
    mov ebx, 1

    l1:
        push ecx
        push ebx
        call imprimir_enter; llamamos a imprimir_enter
        pop ecx
        mov ebx,ecx
        push ebx

    l2:
        push ecx
        call imprimir_asterisco
        pop ecx
        loop l2; exc !0
        pop ebx
        pop ecx
        inc ebx
        loop l1

        ; se termina el programa
        mov eax, 1
        int 80h

    imprimir_asterisco:
        mov eax, 4
        mov ebx, 1
        mov ecx, asterisco
        mov edx, 1
        int 80h
        ret ;se retorna hast la última linea del call

    imprimir_enter:
        mov eax, 4
        mov ebx, 1
        mov ecx, nueva_linea
        mov edx, 1
        int 80h

        ret ;se retorna hast la última linea del call

