section .data
	msj: db 'La suma es:', 0xA
	len: equ $-msj
	
section .bss
	suma: resb 10
	
section .text
	global _start
	
_start:
	mov ax,2
	mov bx,5
	add ax,bx
	add ax,'0'		;Convierte el número a cadena
	mov [suma],ax
	
	mov eax,04
	mov ebx,01
	mov ecx,msj
	mov edx,len
	int 80H
	
	mov eax,04
	mov ebx,01
	mov ecx,suma
	mov edx,10
	int 80H
	
	mov eax,01
	mov ebx,00
	int 80H