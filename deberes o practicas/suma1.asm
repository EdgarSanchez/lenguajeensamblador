section .data
	a db "ingrese un numero", 10
    len_a equ $ - a
    b db "ingrese otro numero", 10
    len_b equ $ - b

    resultado db "La suma es: ", 10
    len equ $ - resultado
    
    newline db "", 10
	
section .bss
	a: resb 1
	b: resb 1
	r: resb 1
	
section .text
	global _start
	
_start:
	mov eax,04
	mov ebx,01
	mov ecx,msj_a
	mov edx,len_a
	int 80H
	
	mov eax,03
	mov ebx,02
	mov ecx,a
	mov edx,2
	int 80H
	
	mov eax,04
	mov ebx,01
	mov ecx,msj_b
	mov edx,len_b
	int 80H
	
	mov eax,03
	mov ebx,02
	mov ecx,b
	mov edx,2
	int 80H
	
	mov ax,[a]
	mov bx,[b]
	sub ax,'0'
	sub bx,'0'
	add ax,bx
	add ax,'0'
	mov [r],ax
	
	mov eax,04
	mov ebx,01
	mov ecx,msj_r
	mov edx,len_r
	int 80H
	
	mov eax,04
	mov ebx,01
	mov ecx,r
	mov edx,1
	int 80H
	
	mov eax,04
	mov ebx,01
	mov ecx,msj_f
	mov edx,len_f
	int 80H
	
	mov eax,01
	mov ebx,00
	int 80H