section .data
	saludo db 'Hola ensamblador', 0x0a
	len equ $-saludo

section .text
	global _start

_start:
	mov eax,04	;ax define el tipo de operación del S.O(4=write)
	mov ebx,01	;bx define la unidad estandar
	mov ecx,saludo ;cx se almacena la referencia de la constante saludo para imprimir
	mov edx,len	;dx define el número de caracteres que se va a imprimir
	int 80H		;interrupción
	mov eax,1	;ax define el tipo de operación (1=exit)
	mov ebx,0
	int 80H