section .data
	%assign signos '*'
	msj db 'Hola clase ', signos, signos, signos, 10
	len equ $-msj
	
section .text
	global _start
	
_start:
	mov eax,04
	mov ebx,01
	mov ecx,msj
	mov edx,len
	int 80h
	
	mov eax,01
	mov ebx,00
	int 80h