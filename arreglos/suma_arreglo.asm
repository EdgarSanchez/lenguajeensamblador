; Nombre: Edgar Patricio Sánchez Malla.
; Ciclo-Paralelo: 6 "B"
section .data
	msg db 'Ingrese un numero y presione enter', 10
	len equ $-msg
	arreglo db 0,0,0,0,0
	len_a equ $-arreglo
	msg_f db 10,'',10
	len_f equ $-msg_f
	
section .bss
	r: resb 2
	
section .text
	global _start
	
_start:
	mov esi,arreglo		
	mov edi,0		

leer:
	mov eax,04
	mov ebx,01
	mov ecx,msg
	mov edx,len
	int 80H

	mov eax,03
	mov ebx,02
	mov ecx,r
	mov edx,2
	int 80H
	
	mov al,[r]
	sub al,'0'
	mov [esi],al
	inc esi
	inc edi
	
	cmp edi,len_a
	jb leer
	call imprimirLine
	mov ecx,0
	mov bl,0

sumar:
	mov al,[arreglo+ecx]
	add bl,al
	jmp contador

contador:
	inc ecx
	cmp ecx,len_a
	jb sumar
	jmp imprimir

imprimir:
	add bl,'0'
	mov [r],bl
	
	mov eax,04
	mov ebx,01
	mov ecx,r
	mov edx,1
	int 80h
	
	jmp exit

imprimirLine:
	mov eax,04
	mov ebx,01
	mov ecx,msg_f
	mov edx,len_f
	int 80h
	
	ret
	
exit:
	call imprimirLine
	mov eax,01
	mov ebx,00
	int 80H
